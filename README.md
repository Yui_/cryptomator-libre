[![cryptomator](cryptomator.png)](https://cryptomator.org/)

 

[![Build](https://github.com/cryptomator/cryptomator/workflows/Build/badge.svg)](https://github.com/cryptomator/cryptomator/actions?query=workflow%3ABuild)
[![Known Vulnerabilities](https://snyk.io/test/github/cryptomator/cryptomator/badge.svg)](https://snyk.io/test/github/cryptomator/cryptomator)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/2a0adf3cec6a4143b91035d3924178f1)](https://www.codacy.com/gh/cryptomator/cryptomator/dashboard)
[![Twitter](https://img.shields.io/badge/twitter-@Cryptomator-blue.svg?style=flat)](http://twitter.com/Cryptomator)
[![Crowdin](https://badges.crowdin.net/cryptomator/localized.svg)](https://translate.cryptomator.org/)
[![Latest Release](https://img.shields.io/github/release/cryptomator/cryptomator.svg)](https://github.com/cryptomator/cryptomator/releases/latest)
[![Community](https://img.shields.io/badge/help-Community-orange.svg)](https://community.cryptomator.org)
[![Dark Status](https://img.shields.io/badge/Dark%20Theme-Enabled-000000)](https://img.shields.io/badge/Dark%20Theme-Always-000000)

 

<h2 align="center">

Cryptomator libre

</h2>

<p align="center">

License go brrr.

</p>

 

## The hell is this?

This is a ridiculously simple fork of cryptomator with one simple change, the code [returns true](https://github.com/g-yui/cryptomator-libre/commit/5ca2b69b6aecc5a68182ebecfe4cbfe1a52dc069) for the license check functions, as such, all current and future functions locked behind the "support" paywall will be unlocked, ***it just works™***.

 

## Why are Linux and MacOS artifacts not available?

Simply put I cannot be bothered to fix the build.yml, if you wish to do so just push a PR!

 

## There aren't any signatures in the binaries, iS tHiS a ViRuS nOw?!

lol

 

## Is this even *legal*?

*Probably*, because of the terms of the FOSS license, since I'm not making any money from this fork while sharing the sauce it doesn't really break the terms of the license.

 

## Where to download?

From the latest working artifact! Right [here](https://github.com/g-yui/cryptomator/actions/workflows/release.yml)

 

### If you have any other questions just open an issue.

 

